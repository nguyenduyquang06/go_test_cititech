# How to install ?
* Step 1: *npm install*
* Step 2: *npm run dev*

# Technology
- Front-end: ReactJS, Bootstrap, Nextjs for routing and server-side rendering
- Database: SQLite
- Back-end: Golang using Gin framework for api server. Running at Google Cloud VPS


# Some images 
![alt text](https://i.imgur.com/Vw6Q7l3.png)