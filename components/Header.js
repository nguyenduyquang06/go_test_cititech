import Link from 'next/link'

const linkStyle = {
  marginRight: 15
}

const Header = () => (
    <div>        
        <a href="http://localhost:3001" style={linkStyle}>Home</a>
        <Link href="/about">
          <a style={linkStyle}>About</a>
        </Link>        
    </div>
)

export default Header
