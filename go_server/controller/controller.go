package controller

import (
	"fmt"
	"go_test_cititech/go_server/model"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

var db *gorm.DB

func SendDb(database *gorm.DB) {
	db = database
}

type RequestDelete struct {
	ProjectID string `json:"projectId"`
	MemberID  string `json:"memberId"`
}
type ResultListProject struct {
	Members      []model.Member `json:"member"`
	AvailMembers []model.Member `json:"availmember"`
}

func ListProjectMember(c *gin.Context) {
	pid := c.Params.ByName("pid")
	var mapping []model.Mapping
	var member model.Member
	var result ResultListProject
	temp := []model.Member{}
	if err := db.Where("project_id = ?", pid).Find(&mapping).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		for _, val := range mapping {
			if err := db.Where("id = ?", val.MemberID).First(&member).Error; err != nil {
				c.AbortWithStatus(404)
				fmt.Println(err)
			} else {
				temp = append(temp, member)
				fmt.Println(member)
				member = model.Member{}
			}
		}
		result.Members = temp
		var allMem []model.Member
		if err := db.Find(&allMem).Error; err != nil {
			c.AbortWithStatus(404)
			fmt.Println(err)
		} else {
			for _, val := range allMem {
				flag := false
				for _, val2 := range temp {
					if val.ID == val2.ID {
						flag = true
						break
					}
				}
				if !flag {
					result.AvailMembers = append(result.AvailMembers, val)
				}
			}
			c.JSON(200, result)
		}
	}
}

func AddProjectMember(c *gin.Context) {
	request := &RequestDelete{}
	request.MemberID = c.Params.ByName("memid")
	request.ProjectID = c.Params.ByName("pid")
	//buf := make([]byte, 1024)
	var mapping model.Mapping
	//num, _ := c.Request.Body.Read(buf)
	//err := json.Unmarshal(buf[:num], request)
	fmt.Println(request.MemberID)
	db.Where("project_id = ? AND member_id = ?", request.ProjectID, request.MemberID).First(&mapping)
	if mapping.ID == 0 {
		val, _ := strconv.Atoi(request.MemberID)
		val2, _ := strconv.Atoi(request.ProjectID)
		mapping.MemberID = uint(val)
		mapping.ProjectID = uint(val2)
		db.Create(&mapping)
	}
	c.JSON(200, gin.H{
		"msg":       "deleted successfully",
		"projectId": request.ProjectID,
		"memberId":  request.MemberID,
	})
}

func DeleteProjectMember(c *gin.Context) {
	request := &RequestDelete{}
	request.MemberID = c.Params.ByName("memid")
	request.ProjectID = c.Params.ByName("pid")
	//buf := make([]byte, 1024)
	var mapping model.Mapping
	//num, _ := c.Request.Body.Read(buf)
	//err := json.Unmarshal(buf[:num], request)
	fmt.Println(request.MemberID)
	db.Where("project_id = ? AND member_id = ?", request.ProjectID, request.MemberID).Delete(&mapping)
	c.JSON(200, gin.H{
		"msg":       "deleted successfully",
		"projectId": request.ProjectID,
		"memberId":  request.MemberID,
	})
}

func UpdateMapping(c *gin.Context) {

	var member model.Member
	id := c.Params.ByName("id")

	if err := db.Where("id = ?", id).First(&member).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	}
	c.BindJSON(&member)

	db.Save(&member)
	c.JSON(200, member)

}

func CreateMapping(c *gin.Context) {

	var mapping model.Mapping
	c.BindJSON(&mapping)

	db.Create(&mapping)
	c.JSON(200, mapping)
}
func GetMapping(c *gin.Context) {
	var mapping []model.Mapping
	if err := db.Find(&mapping).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		c.JSON(200, mapping)
	}
}

func DeleteProject(c *gin.Context) {
	id := c.Params.ByName("id")
	var project model.Project
	d := db.Where("id = ?", id).Delete(&project)
	fmt.Println(d)
	c.JSON(200, gin.H{"id #" + id: "deleted"})
}

func UpdateProject(c *gin.Context) {

	var project model.Project
	id := c.Params.ByName("id")

	if err := db.Where("id = ?", id).First(&project).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	}
	c.BindJSON(&project)

	db.Save(&project)
	c.JSON(200, project)

}

func CreateProject(c *gin.Context) {
	var project model.Project
	c.BindJSON(&project)
	err := db.Create(&project)
	if err != nil {
		c.JSON(403, err)
	} else {
		c.JSON(200, project)
	}
}

func GetProject(c *gin.Context) {
	id := c.Params.ByName("id")
	var project model.Project
	if err := db.Where("id = ?", id).First(&project).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		c.JSON(200, project)
	}
}
func GetProjects(c *gin.Context) {
	var project []model.Project
	if err := db.Find(&project).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		c.JSON(200, project)
	}
}

func UpdateMember(c *gin.Context) {

	var member model.Member
	id := c.Params.ByName("id")

	if err := db.Where("id = ?", id).First(&member).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	}
	c.BindJSON(&member)

	db.Save(&member)
	c.JSON(200, member)

}

func CreateMember(c *gin.Context) {

	var member model.Member
	c.BindJSON(&member)

	err := db.Create(&member)
	if err != nil {
		c.JSON(403, err)
	} else {
		c.JSON(200, member)
	}

}

func GetMember(c *gin.Context) {
	id := c.Params.ByName("id")
	var member model.Member
	if err := db.Where("id = ?", id).First(&member).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		c.JSON(200, member)
	}
}
func GetMembers(c *gin.Context) {
	var member []model.Member
	if err := db.Find(&member).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		c.JSON(200, member)
	}
}

func DeleteMember(c *gin.Context) {
	id := c.Params.ByName("id")
	var member model.Member
	d := db.Where("id = ?", id).Delete(&member)
	fmt.Println(d)
	c.JSON(200, gin.H{"id #" + id: "deleted"})
}
