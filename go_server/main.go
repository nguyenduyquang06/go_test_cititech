package main

// only need mysql OR sqlite
// both are included here for reference
import (
	"fmt"
	"go_test_cititech/go_server/controller"
	"go_test_cititech/go_server/model"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/itsjamie/gin-cors"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

var db *gorm.DB
var err error

func main() {
	// NOTE: See we're using = to assign the global var
	//         	instead of := which would assign it only in this function
	db, err = gorm.Open("sqlite3", "./gorm.db")
	//db, _ = gorm.Open("mysql", "user:pass@tcp(127.0.0.1:3306)/database?charset=utf8&parseTime=True&loc=Local")

	if err != nil {
		fmt.Println(err)
	}
	defer db.Close()
	controller.SendDb(db)
	db.AutoMigrate(&model.Project{})
	db.AutoMigrate(&model.Member{})
	db.AutoMigrate(&model.Mapping{})

	r := gin.Default()
	r.Use(cors.Middleware(cors.Config{
		Origins:         "*",
		Methods:         "GET, PUT, POST, DELETE",
		RequestHeaders:  "Origin, Authorization, Content-Type",
		ExposedHeaders:  "",
		MaxAge:          50 * time.Second,
		Credentials:     true,
		ValidateHeaders: false,
	}))
	r.GET("/project/", controller.GetProjects)
	r.GET("/project/:id", controller.GetProject)
	r.POST("/project", controller.CreateProject)
	r.PUT("/project/:id", controller.UpdateProject)
	r.DELETE("/project/:id", controller.DeleteProject)

	r.GET("/member/", controller.GetMembers)
	r.GET("/member/:id", controller.GetMember)
	r.POST("/member", controller.CreateMember)
	r.PUT("/member/:id", controller.UpdateMember)
	r.DELETE("/member/:id", controller.DeleteMember)

	r.GET("/mapping/", controller.GetMapping)
	r.GET("/mapping/:pid", controller.ListProjectMember)
	r.POST("/mapping", controller.CreateMapping)
	r.PUT("/mapping/:id", controller.UpdateMapping)
	r.GET("/delmem/:pid/:memid", controller.DeleteProjectMember)
	r.GET("/addmemtoproj/:pid/:memid", controller.AddProjectMember)
	r.Run(":9090")
}
