package model

type Project struct {
	ID        uint   `json:"id"`
	Name string `gorm:"unique;not null" json:"name"`
	Description  string `json:"description"`
}

type Member struct {
	ID        uint   `json:"id"`
	Name string `json:"name"`
	Email	string  `gorm:"unique;not null" json:"email"`
}

type Mapping struct {
	ID        uint   `json:"id"`
	ProjectID	uint	`json:"projectId"`
	MemberID uint `json:"memberId"`
	Project Project `gorm:"foreignkey:ProjectID"`
	Member Member `gorm:"foreignkey:MemberID"`
}