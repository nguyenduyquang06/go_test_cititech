import Layout from '../components/MyLayout.js'
import Link from 'next/link'
import fetch from 'isomorphic-unfetch'
import server_api from '../config'
import {Button,ListGroup,ListGroupItem,Modal, ModalHeader, ModalBody, ModalFooter,InputGroup, InputGroupAddon, InputGroupText, Input} from 'reactstrap'

class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {    
      test:"null",
      modal:false,
      newMemName:"Nguyen Duy Quang",
      newMemEmail:"nguyenduyquang06@gmail.com",
      addSuccess:false,
      addFail:false,
      newProjName:"",
      newDescription:"",
      modal2:false,
      projects:this.props.projects
    };            
    
  }

  toggle1 = (e) => {
    this.setState({modal:!this.state.modal})    
    if (e.target.id == "AddMem") {
      this.handleAddMember()
    }
  }
  toggle2 = (e) => {
    this.setState({modal2:!this.state.modal2})    
    if (e.target.id == "AddProj") {
      this.handleAddProject()      
    }
  }

  toggle3 = (e) => {
    this.setState({addSuccess:!this.state.addSuccess})    
  }

  handleAddMember = async () => {
    const res = await fetch(server_api+'/member',{
      method:'post',
      header:{
        "Content-Type": "application/json; charset=utf-8",
      }, 
      body: JSON.stringify({
        "name":this.state.newMemName,
	      "email":this.state.newMemEmail
      })
    })
    const response = await res.json()
    if (response.RowsAffected!=0) {
      console.log("Successfully")
      this.setState({addSuccess:true})
    } else {
      this.setState({addFail:true})
    }
  }

  handleAddProject = async () => {
    const res = await fetch(server_api+'/project',{
      method:'post',
      header:{
        "Content-Type": "application/json; charset=utf-8",
      }, 
      body: JSON.stringify({
        "name":this.state.newProjName,
	      "description":this.state.newDescription
      })
    })    
    const response = await res.json()
    if (response.RowsAffected!=0) {
      console.log("Successfully")
      this.setState({addSuccess:true})
      window.location.href = ''
    } else {
      this.setState({addFail:true})
    }
  }

  render() {
    return (
      <Layout>
    <h1>List Available Projects</h1>
  <ListGroup>
    {this.props.projects.map( project => (
        <ListGroupItem className="list-group-item" key={project.id}>
          <Link as={`/p/${project.id}`} href={`/project?id=${project.id}`}>
            <a>{project.name}</a>
          </Link>
        </ListGroupItem>
    ))}
      </ListGroup>
      <div>
      <Button id="AddMember" onClick={this.toggle1} color="danger">Add member</Button>
      <Modal isOpen={this.state.modal} toggle={this.toggle1} className={this.props.className}>
          <ModalHeader toggle={this.toggle1}>Add member</ModalHeader>
          <ModalBody>
            <InputGroup>
              <InputGroupAddon addonType="prepend">Name:</InputGroupAddon>
              <Input onChange={(e) => this.setState({newMemName:`${e.target.value}`})} placeholder="for ex: Nguyen Duy Quang" />
            </InputGroup>
            <InputGroup>
              <InputGroupAddon addonType="prepend">Email:</InputGroupAddon>
              <Input onChange={(e) => this.setState({newMemEmail:`${e.target.value}`})} placeholder="for ex: nguyenduyquang06@gmail.com" />
            </InputGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" id="AddMem" onClick={this.toggle1}>Add</Button>{' '}
            <Button color="secondary" id="Cancel"  onClick={this.toggle1}>Cancel</Button>
          </ModalFooter>
      </Modal>
      <Modal isOpen={this.state.addSuccess} toggle={this.toggle3} className={this.props.className}>
          <ModalHeader toggle={this.toggle3}>Add Successfully</ModalHeader>
          <ModalFooter>
            <Button color="primary" onClick={this.toggle3}>Ok</Button>{' '}            
          </ModalFooter>
      </Modal>
      <Modal isOpen={this.state.addFail} className={this.props.className}>
          <ModalHeader>Add Fail</ModalHeader>
          <ModalFooter>
            <Button color="primary" onClick={() => this.setState({addFail:false})}>Ok</Button>{' '}            
          </ModalFooter>
      </Modal>
      </div>
      <div>
        <Button id="btnAddProj" onClick={this.toggle2} color="warning">Add project</Button>
        <Modal isOpen={this.state.modal2} toggle={this.toggle2} className={this.props.className}>
          <ModalHeader toggle={this.toggle2}>Add new project</ModalHeader>
          <ModalBody>
            <InputGroup>
              <InputGroupAddon addonType="prepend">Project:</InputGroupAddon>
              <Input onChange={(e) => this.setState({newProjName:`${e.target.value}`})} placeholder="for ex: Project X" />
            </InputGroup>
            <InputGroup>
              <InputGroupAddon addonType="prepend">Description:</InputGroupAddon>
              <Input onChange={(e) => this.setState({newDescription:`${e.target.value}`})} placeholder="for ex: hello world" />
            </InputGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" id="AddProj" onClick={this.toggle2}>Add</Button>{' '}
            <Button color="secondary" id="Cancel"  onClick={this.toggle2}>Cancel</Button>
          </ModalFooter>
      </Modal>
      </div>      
  </Layout>
    )
  }
}

Index.getInitialProps = async function() {
  const res = await fetch(server_api+`/project`)
  const data = await res.json()

  console.log(`project data fetched. Count: ${data.length}`)

  return {
    projects: data
  }
}

export default Index