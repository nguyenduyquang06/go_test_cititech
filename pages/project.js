import Layout from '../components/MyLayout.js'
import React from 'react'
import server_api from '../config'
import fetch from 'isomorphic-unfetch'
import {Label,Input,Form,FormGroup,Button,ListGroup,ListGroupItem,ListGroupItemHeading,ListGroupItemText} from 'reactstrap';

class Project extends React.Component {
  constructor(props) {
    super(props);
    var result
    if (this.props.members.availmember != null) {
      result = this.props.members.availmember[0].id
    } else {
      result =0
    }
    this.state = {      
      members:this.props.members,
      modal: false,
      backdrop: true,
      member:result
    };        
    this.changeMember = this.changeMember.bind(this);
  }

  handleAddMember = async () => {
    console.log(this.props.id)
    const res = await fetch(server_api+`/addmemtoproj/${this.props.id}/${this.state.member}`)    
    const res_member = await fetch(server_api+`/mapping/${this.props.id}`)
    const members = await res_member.json()
    if (members.availmember==null) {
      this.setState({members,member:0})    
    } else {
      this.setState({members,member:members.availmember[0].id})    
    }
  }

  changeMember(e) {
    let value = e.target.value
    this.setState({member:value})
  }

  handleDelMember = async (id) => {
    console.log(this.props.id)    
    const res = await fetch(server_api+`/delmem/${this.props.id}/${id}`)    
    const res_member = await fetch(server_api+`/mapping/${this.props.id}`)
    const members = await res_member.json()
    this.setState({members,member:members.availmember[0].id})
  }

  handleTest = () => {
    if (this.state.member==0) {
        return (
          <option>Not Available</option>
        )
    } else {
      return (
        this.state.members.availmember.map( member => (
          <option key={member.id} value={member.id}>{member.name}</option>
        ))
      );
    }
  }

  render() {
    return (
      <Layout>
       <h1>{this.props.project.name}</h1>
       <p>Description: {this.props.project.description}</p> 
      <ListGroup>
    {this.state.members.member.map( member => (
        <ListGroupItem className="list-group-item" key={member.id}>
        <ListGroupItemHeading>
            {member.name}
          </ListGroupItemHeading>          
          <ListGroupItemText>
          Email: {member.email}
            </ListGroupItemText>   
            <Button value={member.id} onClick={ () => this.handleDelMember(member.id)} color="primary">Delete member</Button>{' '}
        </ListGroupItem>
    ))}
      </ListGroup>
      <Form inline onSubmit={(e) => e.preventDefault()}>
          <FormGroup>
            <Label for="backdrop">Add member to project: </Label>{' '}
            <Input type="select" name="backdrop" id="backdrop" onChange={this.changeMember}>
              {this.handleTest()}
            </Input>
          </FormGroup>
          {' '}
          <Button onClick={() => this.handleAddMember()} color="danger">Add member</Button>
        </Form>
    </Layout>
    )
  }
}

Project.getInitialProps = async function (context) {
  const { id } = context.query  
  const res_member = await fetch(server_api+`/mapping/${id}`)
  const res_proj = await fetch(server_api+`/project/${id}`)  
  const project = await res_proj.json()
  const members = await res_member.json()

  console.log(`Fetched project: ${project.name}`)

  return { project,members,id }
}

export default Project